import java.util.Random;
import java.util.Scanner;

public class Manager {
    public static boolean isOn = true;

    public void help() {
        System.out.println("*Available commands");
        System.out.println("1. End turn");
        System.out.println("2. Status");
        System.out.println("3. Clear");
        System.out.println("4. Stop");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Hello, please enter the number of parking spaces for cars.");
        int spacesA = scanner.nextInt();
        Parking parkingSpacesA = new Parking(spacesA, "auto");
        System.out.println("Enter the number of places for trucks");
        int spacesT = scanner.nextInt();
        Parking parkingSpacesT = new Parking(spacesT, "trucks");

        Random random = new Random();
        Manager manager = new Manager();
        int id = 1000;
        parkingSpacesA.add();
        parkingSpacesT.add();
        while (isOn == true) {
            manager.help();
            System.out.println("Enter the command number");
            int command = scanner.nextInt();
            switch (command) {
                case 1: {
                    int avto = random.nextInt((spacesA + spacesT + 1) / 3);

                    while (avto > 0) {

                        Car auto = new Car(id, "auto");
                        if (parkingSpacesA.getFreePlaces() > 0) {

                            int steps = random.nextInt(10) + 1;
                            int k = parkingSpacesA.returnFreePlaces();
                            parkingSpacesA.park(auto, steps, k);
                            System.out.println("You successfully parked auto identified as " + id + " on place -" + auto.getPlace() + "- for " + auto.getSteps() + " steps");


                        } else {
                            System.out.println("Sorry, passenger auto seats are occupied");
                            System.out.println("Please, wait " + parkingSpacesA.getBusyPlaces() + " steps");
                            break;

                        }
                        parkingSpacesA.updateValues();
                        id++;
                        avto--;

                    }
                    int trucks = random.nextInt((spacesA + spacesT + 1) / 3);

                    while (trucks > 0) {

                        Car truck = new Car(id, "truck");

                        if (parkingSpacesT.getFreePlaces() > 0) {

                            int steps = random.nextInt(10) + 1;
                            int k = parkingSpacesT.returnFreePlaces();
                            parkingSpacesT.park(truck, steps, k);
                            System.out.println("You successfully parked truck identified as " + id + " on place -" + truck.getPlace() + "- for " + truck.getSteps() + " steps");
                        } else if (parkingSpacesA.isAvailablePlaceForTruck() > 0) {
                            int steps = random.nextInt(10) + 1;
                            int place = parkingSpacesA.isAvailablePlaceForTruck();
                            parkingSpacesA.parkTruckOnCar(truck, steps, place);
                            parkingSpacesA.parkTruckOnCar(truck, steps, place + 1);
                            System.out.println("You successfully parked truck identified as " + id + " on place -" + truck.getPlace() + "- and -" + (truck.getPlace() + 1) + "- for " + truck.getSteps() + " steps on car parking");
                        } else {
                            System.out.println("Sorry, truck seats are occupied");
                            System.out.println("Please, wait " + parkingSpacesT.getBusyPlaces() + " steps");
                            break;
                        }
                        parkingSpacesA.updateValues();
                        parkingSpacesT.updateValues();
                        id++;
                        trucks--;
                    }
                    parkingSpacesA.endRound();
                    parkingSpacesT.endRound();
                    parkingSpacesA.updateValues();
                    parkingSpacesT.updateValues();
                    break;
                }
                case 4: {
                    isOn = false;
                    break;
                }
                case 2: {
                    parkingSpacesA.enter();
                    parkingSpacesT.enter();
                    break;

                }
                case 3:
                    System.out.println("Choose parking");
                    String type = scanner.next();
                    switch (type) {
                        case ("auto"):
                            parkingSpacesA.clearParking();
                            System.out.println("Auto parking is empty now!");
                            break;

                        case ("truck"):
                            parkingSpacesT.clearParking();
                            System.out.println("Truck parking is empty now!");
                            break;

                    }
                    break;


            }


        }

    }
}


