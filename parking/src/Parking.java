import java.util.ArrayList;

public class Parking {
    private int parkingSpaces;
    private String parkingType;
    private int freePlaces;
    private int busyPlaces;
    ArrayList<Car> carPlaces;

    public Parking(int parkingSpaces, String parkingType) {
        this.parkingSpaces = parkingSpaces;
        this.parkingType = parkingType;
        this.carPlaces = new ArrayList<Car>();
        this.freePlaces = parkingSpaces;
        this.busyPlaces = 0;
    }

    public int getParkingSpaces() {
        return parkingSpaces;
    }

    public void setParkingSpaces(int parkingSpaces) {
        this.parkingSpaces = parkingSpaces;
    }

    public String getParkingType() {
        return parkingType;
    }

    public void setParkingType(String parkingType) {
        this.parkingType = parkingType;
    }

    public int getFreePlaces() {
        return freePlaces;
    }

    public void setFreePlaces(int freePlaces) {
        this.freePlaces = freePlaces;
    }

    public int getBusyPlaces() {
        return busyPlaces;
    }

    public void setBusyPlaces(int busyPlaces) {
        this.busyPlaces = busyPlaces;
    }

    public int returnFreePlaces() {
        for (int i = 0; i < this.parkingSpaces; i++) {
            if (this.carPlaces.get(i) == null) {
                return i;
            }
        }
        return -1;
    }

    public void park(Car car, int steps, int k) {
        this.carPlaces.set(k, car);
        this.carPlaces.get(k).setSteps(steps);
        this.carPlaces.get(k).setPlace(k + 1);
    }

    public void updateValues() {
        int minBusyPlaces = 10;
        int places = 0;
        for (int j = 0; j < this.parkingSpaces; j++) {
            if (this.carPlaces.get(j) != null) {
                int b = carPlaces.get(j).getSteps();
                if (b == 0) {
                    this.carPlaces.get(j).setPlace(0);
                    this.carPlaces.set(j, null);
                } else if (b <= minBusyPlaces) {
                    minBusyPlaces = b;
                }
            } else {
                places++;
            }
        }
        if (places != 0) {
            setBusyPlaces(0);
        } else {
            setBusyPlaces(minBusyPlaces);
        }
        setFreePlaces(places);
    }

    public int isAvailablePlaceForTruck() {
        if (this.parkingType.equals("auto")) {
            for (int j = 0; j < this.parkingSpaces - 1; j++) {
                if (this.carPlaces.get(j) == null) {
                    if (this.carPlaces.get(j+1) == null) {
                        return j;
                    }
                }
            }
            return -1;
        }
        return -1;

    }

    public void parkTruckOnCar(Car car, int steps, int k) {
        this.carPlaces.set(k, car);
        this.carPlaces.get(k).setSteps(steps);
        this.carPlaces.get(k).setPlace(k + 1);
    }

    public void endRound(){
        for(int j = 0; j < this.parkingSpaces; j++) {
            if(this.carPlaces.get(j) != null){
                this.carPlaces.get(j).setSteps(this.carPlaces.get(j).getSteps()-1);
                if(getParkingType().equals("auto")&(this.carPlaces.get(j).getType().equals("truck"))){
                    j++;
                }
            }
        }
    }

    public void enter(){
        System.out.println(getParkingType() + "-parking:");
        System.out.println("----------------");
        for(int j = 0; j < this.parkingSpaces; j++) {
            if (this.carPlaces.get(j) == null) {
                System.out.println("№" + (j+1) + " - empty");
            } else {
                System.out.println("№" + (j+1) + " - " + this.carPlaces.get(j).getType() + " (id " + this.carPlaces.get(j).getId() + ") for " + this.carPlaces.get(j).getSteps() + " steps");
            }
        }
        System.out.println("");

    }
    public void clearParking(){
        for(int j = 0; j < this.parkingSpaces; j++){
            if(this.carPlaces.get(j) != null) {
                this.carPlaces.get(j).setSteps(0);
                this.carPlaces.get(j).setPlace(0);

                this.carPlaces.set(j, null);
            }
        }
    }

    public void add(){
        for(int j = 0; j < this.parkingSpaces; j++) {
            this.carPlaces.add(null);
        }
    }


}
